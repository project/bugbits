<?php
module_load_include('inc', 'bugbits', 'includes/mantisClient');
class mantisIssue{
  static function get($issueId){
    $client = mantisClient::get();
    return $client->mc_issue_get($client->user, $client->pass, $issueId);
  }
  static function add(){}
  static function update(){}
  static function delete(){}
  static function noteAdd(){}
  static function noteDelete(){}
  static function relationshipAdd(){}
  static function relationshipDelete(){}
}
