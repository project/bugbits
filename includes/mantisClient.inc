<?php 
class mantisClient{
  private static $instance;
  private $client;
  // TODO: make sure these variables are private (don't expose password!), soap calls that expect username and password should use other method (e.g. by default including them in the call here)
  public $user;
  public $pass;
  
  /**
   * Private constructor via the singleton pattern
   * http://php.net/manual/en/language.oop5.patterns.php
   *
   * @return unknown_type
   */
  private function __construct(){
    $this->user = variable_get('bugbits_mantis_admin_user', false);
    $this->pass = variable_get('bugbits_mantis_admin_pass', false);
    $mantis_url = variable_get('bugbits_mantis_url', false);
    $this->client = new SoapClient($mantis_url ? $mantis_url . '/api/soap/mantisconnect.php?wsdl' : false);
  }
  
  /**
   * Use this function to get the only instance of the client
   *
   * @return an object of type client
   */
  public static function get(){
    if(!isset(self::$instance)){
      $c = __CLASS__;
      self::$instance = new $c;
    }
    return self::$instance;
  }
  
  /**
   * 
   * @param $name: the name of the function to be called via soap
   * @param $arguments: the arguments array
   * @return mixed: depending on the return type specified in the wsdl
   */
  public function __call($name, $arguments){
    try{
     $return = $this->client->__call($name, $arguments);
    }catch(Exception $exception){
      watchdog('Bugbits: Mantis Connect Soap Api', $exception->faultstring, array(), WATCHDOG_WARNING); 
      $return = false;
    }
    return $return;
  }
}
