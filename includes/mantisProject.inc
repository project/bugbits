<?php
module_load_include('inc', 'bugbits', 'includes/mantisClient');
class mantisProject{
  /**
   * 
   * @param $name
   * @param $desciption
   * @param $status
   * @param $viewState
   * @param $enabled
   * @return int of project id
   */
  static function add($name, $description, $status = 10, $viewState = 50, $enabled = TRUE){
    $client = mantisClient::get();
    return $client->mc_project_add(
      $client->user, 
      $client->pass,
      array(
        'name' => $name,
        'status' => $status,
        'view_state' => $viewState,
        'enabled' => $enabled,
        'description' => $description
      )
    ); 
  }
  /**
   * 
   * @param $id: the project id
   * @return unknown_type
   */
  static function delete($projectId){
    $client = mantisClient::get();
    return $client->mc_project_delete($client->user, $client->pass, $projectId);  
  }
  
  /**
   * 
   * @param $name
   * @return int of project id
   */
  static function getId($name){
    $client = mantisClient::get();
    return $client->mc_project_get_id_from_name($client->user, $client->pass, $name);
  }
  
  /**
   * 
   * @param $projectId
   * @param $page
   * @param $limit
   * @return an array of issue objects
   */
  static function getIssues($projectId, $page = 0, $limit = 10){
    $client = mantisClient::get();
    return $client->mc_project_get_issues($client->user, $client->pass, $projectId, $page, $limit);
  }
  
  /**
   * 
   * @param $projectId
   * @param $page
   * @param $limit
   * @return array of issue header objects
   */
  static function getIssueHeaders($projectId, $page = 0, $limit = 10){
    $client = mantisClient::get();
    return $client->mc_project_get_issue_headers($client->user, $client->pass, $projectId, $page, $limit);
  }
  
  /**
   * Get all the projects identified in mantis
   * @param $name
   * @return int of project id
   */
  static function getAllProjects(){
    // TODO: not enabled projects are not given here
    $client = mantisClient::get();
    return $client->mc_projects_get_user_accessible($client->user, $client->pass);
  }
}
