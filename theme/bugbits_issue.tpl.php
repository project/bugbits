<!--  TODO: need to deal with timestamps -->
<?php print l('Back to Project', "mantis/project/{$issue->project->id}/issues")?>
<table cellspacing="1">
  <tbody>
    <tr>
      <td class="category" width="15%">ID</td>
      <td class="category" width="20%">Project</td>
      <td class="category" width="15%">Category</td>
      <td class="category" width="15%">View Status</td>
      <td class="category" width="15%">Date Submitted</td>
      <td class="category" width="20%">Last Update</td>
    </tr>
    <tr class="row-1">
      <td><?php print $issue->id;?></td>
      <td><?php print $issue->project->name;?></td>
      <td><?php print $issue->category;?></td>
      <td><?php print $issue->view_state->name;?></td>
      <td><?php $d = new DateTime($issue->date_submitted); print $d->format('Y-m-d G:i'); ?></td>
      <td><?php $d = new DateTime($issue->last_updated); print $d->format('Y-m-d G:i'); ?></td>
    </tr>
      <tr class="spacer">
      <td colspan="6"></td>
    </tr>
    <tr class="row-2">
      <td class="category">Reporter</td>
      <td><?php print $issue->reporter->name; ?></td>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr class="row-1">
      <td class="category">Assigned To</td>
      <td><?php print $issue->handler->name; ?></td>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr class="row-2">
      <td class="category">Priority</td>
      <td><?php print $issue->priority->name; ?></td>
      <td class="category">Severity</td>
      <td><?php print $issue->severity->name; ?></td>
      <td class="category">Reproducibility</td>
      <td><?php print $issue->reproducibility->name; ?></td>
    </tr>
    <tr class="row-1">
      <td class="category">Status</td>
      <td><?php $issue->status->name; ?></td>
      <td class="category">Resolution</td>
      <td><?php print $issue->resolution->name; ?></td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="row-2">
      <td class="category">Platform</td>
      <td><?php print $issue->platform;?></td>
      <td class="category">OS</td>
      <td><?php $issue->os;?></td>
      <td class="category">OS Version</td>
      <td><?php print $issue->os_build; ?></td>
    </tr>
    <tr class="row-1">
      <td class="category">Product Version</td>
      <td><?php print $issue->version; ?></td>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr class="row-2">
      <td class="category">Target Version</td>
      <td><?php print $issue->target_version; ?></td>
      <td class="category">Fixed in Version</td>
      <td><?php print $issue->fixed_in_version; ?></td>
      <td colspan="2">&nbsp;</td>
    </tr>
      <tr class="spacer">
      <td colspan="6"></td>
    </tr>
    <tr class="row-1">
      <td class="category">Summary</td>
      <td colspan="5"><?php print $issue->summary; ?></td>
    </tr>
    <tr class="row-2">
      <td class="category">Description</td>
      <td colspan="5"><?php print $issue->description; ?></td>
    </tr>
  </tbody>
</table>