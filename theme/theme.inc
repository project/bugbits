<?php
function template_preprocess_bugbit_project_issues(&$vars){

}
function theme_bugbits_project_issue($issues){
  $headers = array(
    "Issue Id",
    "Summary",
    "Category",
    "Priority",
    "Severity",
    "Status",
  );
  $rows = array();
  foreach($issues as $issue){
    $row = array();
    $row[] = l("Issue " . $issue->id, "mantis/issue/" . $issue->id);
    $row[] = $issue->summary;
    $row[] = $issue->category;
    $row[] = $issue->priority->name;
    $row[] = $issue->severity->name;
    $row[] = $issue->status->name . " ({$issue->handler->name})";
    $rows[] = $row;
  }
  return theme_table($headers, $rows);
}