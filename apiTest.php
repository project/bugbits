<?php 
/**
 * This is a temporary file to test and show examples of 
 * how to work with the API.
 */
  /** Temporary function until we integrate with drupal **/
  function variable_get($key, $default){
    $value = false;
    switch($key){
      case 'bugbits_mantis_url':
        $value = 'http://mantis.localhost/api/soap/mantisconnect.php?wsdl';
        break;
      case 'bugbits_mantis_admin_user':
        $value = 'admin';
        break;
      case 'bugbits_mantis_admin_pass':
        $value = 'abc123';
        break;
    }
    return $value;
  }
  require_once('includes/mantisClient.inc');
  require_once('includes/mantisProject.inc');
  require_once('includes/mantisIssue.inc');
  require_once('includes/mantisFilter.inc');
  /**
  $projectId = mantisProject::add('Soap project 23', 'A project I created via soap');
  var_dump($projectId);
  $result = mantisProject::delete(22);
  var_dump($result);
  **/
  $issues = mantisProject::getIssues(10, 0, 1);
  var_dump($issues);
  $issues = mantisProject::getIssueHeaders(10, 0, 1);
  var_dump($issues);
?>