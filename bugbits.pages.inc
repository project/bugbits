<?php

/**
 * @file
 * General page callbacks for the bugbits module.
 */

/**
 * Page callback: provides a list of issues for the given project
 *
 * @param $projectId the mantis project id
 * @return themed html output
 */
function bugbits_project_issues($projectId){
  // TODO: Need to somehow get a count of the issues plus get the name of the project.
  module_load_include('inc', 'bugbits', 'includes/mantisProject');
  
  $page = (isset($_GET['page']) && is_numeric($_GET['page']))? $_GET['page'] : 0;
  $limit = (isset($_GET['limit']) && is_numeric($_GET['limit']))? $_GET['limit'] : 0;
  $issues = mantisProject::getIssues($projectId, $page, $limit);
  
  return ($issues && !empty($issues)) ? theme('bugbits_project_issue', $issues) : "<p>No issues found in this project</p>";  
}

/**
 * Page callback: Provides information for a particular issue
 *
 * @param $issueId the mantis issue id
 * @return themed html output
 */
function bugbits_issue($issueId){
  module_load_include('inc', 'bugbits', 'includes/mantisIssue');  
  $issue = mantisIssue::get($issueId);
  if($issue){
    drupal_set_title($issue->summary);
    $output = theme('bugbits_issue', $issue);
  }else{
    drupal_set_title('Issue not found');
    $output = '<p>Issue not found</p>';
  }
  return $output;
}