<?php

/**
 * @file
 * Admin page callbacks for the bugbits module.
 */

/**
 * Form builder; Configure the Bugbit module
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function bugbits_admin_settings(){
  $form = array();
  
  $form['bugbits_mantis_url'] = array(
    '#title'          => t('Mantis Url'),
    "#description"    => t('Please enter the url that is hosting your mantis installation.  Please do not include a trailing slash.'),
    '#type'           => 'textfield',
    '#default_value'  => variable_get('bugbits_mantis_url',''),
  );
  $form['bugbits_mantis_admin_user'] = array(
    '#title'          => t('Mantis Administrator user name'),
    "#description"    => t('Please enter a mantis account that has administrator privileges.'),
    '#type'           => 'textfield',
    '#default_value'  => variable_get('bugbits_mantis_admin_user',''),
  );
  $form['bugbits_mantis_admin_pass'] = array(
    '#title'          => t('Mantis Administrator password'),
    "#description"    => t('Please enter a password for the account entered above. By default, this field will always show as empty, so your password can not be seen.'),
    '#type'           => 'password',
    '#default_value'  => variable_get('bugbits_mantis_admin_pass',''),
  );
  
  return system_settings_form($form);
}

/**
 * Form builder; Lists the mantis projects and allows to select which ones to show in the drupal site
 *
 * @ingroup forms
 */
function bugbits_admin_projects(){
  module_load_include('inc', 'bugbits', 'includes/mantisProject');
  
  $projects = mantisProject::getAllProjects();
  
  foreach($projects as $project){
    $options[$project->id] = l('Project ' . $project->name, 'mantis/project/' . $project->id . '/issues');
  }
  
  $form['bugbits_selected_projects'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Projects'),
    '#options' => $options,
    '#default_value' => variable_get('bugbits_selected_projects', array()),
  );
  
  return system_settings_form($form);
}


function bugbits_admin_projects_submit(&$form, $form_state){
  
}